declare const jQuery: any
declare const ScrollReveal: any

(($: any) => {
  "use strict"
  $(document).ready(function () {
    // top bar menu
    const $topBar = $('.top-bar');
    const $controls = $topBar.find('[data-control]');

    $controls.each(function(){
      $(this).bind('click', function(e: any){
        var $e = $(e.currentTarget);
        // console.log($e);
        $controls.not($e)
          .each(function(){
            const $el = $(this);
            console.log($el);
            if($el.attr('aria-expanded') == 'true'){
              // $el.trigger('click');
              $el.attr('aria-pressed', 'false');
              $el.attr('aria-selected', 'false');
              $el.attr('aria-expanded', 'false');
              
              $($el.data('control')).removeClass('active');
              $($el.data('control')).attr('aria-expanded', 'false');
              
              $el.removeClass('active');
              
              if($el.find('input')) {
                $el.find('input').focus();
              }
            }
          });

        $e.toggleClass('active');
        $e.attr('aria-pressed',  function (i: any, attr: any) {
          return attr == 'true' ? 'false' : 'true';
        });
        $e.attr('aria-selected',  function (i: any, attr: any) {
          return attr == 'true' ? 'false' : 'true';
        });
        $e.attr('aria-expanded',  function (i: any, attr: any) {
          return attr == 'true' ? 'false' : 'true';
        });
        $($e.data('control')).attr('aria-expanded',  function (i: any, attr: any) {
            return attr == 'true' ? 'false' : 'true';
        });
        $($e.data('control')).toggleClass('active');
        $e.off('mouseenter mouseleave');
      });
    });
  });
  
  
  /*
  *	Sample tag
  *	<header id="rand-images">
  *        Random
  *      Backgrounds
  *	</header>
  *
  *
  */
  var randomImages = (function(){
    'use strict';
    return{
      init: function(){
        this.events();// init events
      },
      events: function(){
        /* random images snippet*/
        var images = [
          'https://irsc.blackboard.com/bbcswebdav/xid-15273956_1',
          'https://irsc.blackboard.com/bbcswebdav/xid-14463256_1',
          'https://irsc.blackboard.com/bbcswebdav/xid-15273960_1',
          'https://irsc.blackboard.com/bbcswebdav/xid-15273961_1',
          'https://irsc.blackboard.com/bbcswebdav/xid-15273958_1',
          'https://irsc.blackboard.com/bbcswebdav/xid-15686662_1', 
          'https://irsc.blackboard.com/bbcswebdav/xid-15686661_1', 
          'https://irsc.blackboard.com/bbcswebdav/xid-15273964_1'
        ];
        // create style tag in head
        var s = document.createElement('style');
        // add id
        s.id = 'random-Img';
        // type css
        s.type = 'text/css';
        // append in head
        document.head.appendChild(s);
        // math random
        var r = images[Math.floor(Math.random() * images.length)];
        // add into style
        s.textContent = '.bgimage{background:url('+r+') no-repeat center center fixed #eee;background-size:cover;-moz-background-size:cover;}';
      }
    };
  })();
  // Ready for the war
  randomImages.init();
  
  
     


















  // jQuery for page scrolling feature - requires jQuery Easing plugin
  $('a.page-scroll').bind('click', (event: any) => {
    let $anchor: any

    /*
      Scroll does not work with path or svgs, *Hack*
    */
    let _tagName = event.target.tagName
    if (['path', 'svg'].includes(_tagName)) {
      $anchor = $('#logo')
    } else {
      $anchor = $(event.target)
    }

    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top - 50)
    }, 1250, 'easeInOutExpo')
    event.preventDefault()
  })

  // Highlight the top nav as scrolling occurs
  // $('body').scrollspy({
  //   target: '.navbar-fixed-top',
  //   offset: 51
  // })

  // Closes the Responsive Menu on Menu Item Click
  // $('.navbar-collapse ul li a').click(() => {
  //   $('.navbar-toggle:visible').click()
  // })

  // Offset for Main Navigation
  // $('#mainNav').affix({
  //   offset: {
  //     top: 100
  //   }
  // })

  // Initialize and Configure Scroll Reveal Animation
  let sr: any = ScrollReveal()
  sr.reveal('.sr-icons', {
    duration: 600,
    scale: 0.3,
    distance: '0px'
  }, 200)
  sr.reveal('.sr-button', {
    duration: 1000,
    delay: 200
  })
  sr.reveal('.sr-contact', {
    duration: 600,
    scale: 0.3,
    distance: '0px'
  }, 300)

  // Initialize and Configure Magnific Popup Lightbox Plugin
  $('.popup-gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
    }
  })

  $('img.svg').each(function() {
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    $.get(imgURL, function(data: any) {
      // Get the SVG tag, ignore the rest
      var $svg = jQuery(data).find('svg');

      // Add replaced image's ID to the new SVG
      if (typeof imgID !== 'undefined') {
        $svg = $svg.attr('id', imgID);
      }
      // Add replaced image's classes to the new SVG
      if (typeof imgClass !== 'undefined') {
        $svg = $svg.attr('class', imgClass + ' replaced-svg');
      }

      // Remove any invalid XML tags as per http://validator.w3.org
      $svg = $svg.removeAttr('xmlns:a');

      // Check if the viewport is set, else we gonna set it if we can.
      if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
        $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
      }

      // Replace image with new SVG
      $img.replaceWith($svg);

    }, 'xml');

  });

})(jQuery)
