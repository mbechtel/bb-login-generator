<!doctype html>
<html lang="en-US">
    <head>
        <title>IRSC Blackboard Learn</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta id="request-method" name="request-method" content="GET">
        <meta name="author" content="Blackboard">
        <meta name="copyright" content="&copy; 1997-2014 Blackboard Inc. All Rights Reserved. U.S. Patent No. 7,493,396 and 7,558,853. Additional Patents Pending.">
        <meta name="keywords" content="Blackboard">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" type="image/x-icon" href="/ui/bb-icon2.ico">
        <link rel="stylesheet" type="text/css" href="/common/shared.css?v=9.1.130093.0-9" id="css_0">
        <!--<link rel="stylesheet" type="text/css" href="/themes/as_2012/theme.css?v=9.1.130093.0-9" id="css_1">-->
        <link rel="stylesheet" type="text/css" media="print" href="/ui/styles/print.css?v=9.1.201404.160205">

        <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="//cdnjs.cloudflare.com/ajax/libs/bootswatch/3.2.0+2/cyborg/bootstrap.css" rel="stylesheet" type="text/css" media="all">
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://blackboard.irsc.edu/bbcswebdav/xid-1824881_1"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Custom CSS -->
        <link href="https://blackboard.irsc.edu/bbcswebdav/xid-1782926_1" rel="stylesheet">

        <!-- Custom Fonts -->
        <!--<link href="https://blackboard.irsc.edu/bbcswebdav/xid-1783330_1" rel="stylesheet" type="text/css">-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

        <%@ taglib uri="/bbNG" prefix="bbNG" %>
        <%@ taglib uri="/bbUI" prefix="bbUI" %>
        <%@ taglib uri="/loginUI" prefix="loginUI" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
        <c:set var="productName" value="${ loginUI:getProductName() }" />

        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-18770504-2']);
            _gaq.push(['_setDomainName', 'irsc.edu']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>

        <style>
            #loginAnnouncements a {
                color: #004990;
            }

            .social-buttons {
                text-align: center;
            }

            .jumbotron p {
              margin-bottom: 0;
              font-size: inherit;
              font-weight: inherit;
            }
        </style>

    </head>
    <body id="page-top" class="index">

      <!-- Navigation -->
      <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header page-scroll">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                  <!--<a class="navbar-brand page-scroll" href="#page-top"><img style="margin-top: -15px;" src="https://blackboard.irsc.edu/bbcswebdav/xid-1788091_1" alt="IRSC"></a>-->
                  <!--<a class="navbar-brand page-scroll" href="#page-top"><img style="margin-top: -15px;" src="https://blackboard.irsc.edu/bbcswebdav/xid-1775213_1" alt="IRSC"></a>-->
                  <a class="navbar-brand page-scroll" href="#page-top"><img style="margin-top: -15px;" src="https://blackboard.irsc.edu/bbcswebdav/xid-1792086_1" alt="IRSC"></a>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-left">
                      <li>
                          <a href="https://twitter.com/VCIRSC" target="_blank"><i class="fa fa-twitter"></i></a>
                      </li>
                      <li><a href="https://www.facebook.com/VCIRSC/" target="_blank"><i class="fa fa-facebook"></i></a>
                      </li>
                  </ul>
                  <ul class="nav navbar-nav navbar-right">

                      <li class="hidden">
                          <a href="#page-top"></a>
                      </li>

                      <li id="alerts" class="hidden">
                          <a id="show-bb-maintenace"
                             style="margin-right: 0.5em"
                             type="button"
                             class="btn btn-danger btn-sm"
                             data-toggle="modal"
                             data-target="#bb-messages">
                             Alerts! <span id="alerts-count"></span>
                           </a>
                      </li>

                      <li>
                          <a class="page-scroll" href="#login">Login</a>
                      </li>

                      <li>
                          <a class="page-scroll" href="#support">Support</a>
                      </li>

                      <li>
                          <a class="page-scroll" href="#announcements">System Announcements</a>
                      </li>
                  </ul>
              </div>
              <!-- /.navbar-collapse -->
          </div>
          <!-- /.container-fluid -->
      </nav>


      <!-- Header -->
      <header style="width: 100%; margin-top: -20px;">
          <div class="container">
              <div class="account-wall">
                  <section id="login" style="margin: 0px; padding: 0;height:150px;"></section>
                  <h1 class="text-center login-title">Sign in to continue to Blackboard Learn</h1>
                  <img class="profile-img" src="https://blackboard.irsc.edu/bbcswebdav/xid-1778872_1" alt="" />
                  <form class="form-signin" role="form" action="/webapps/login/" onsubmit="return validate_form( this, false, true );" method="POST" name="login" >
                      <input type="text" name="user_id" id="user_id"  class="form-control" placeholder="Username" required autofocus>
                      <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                      <label class="checkbox pull-left">
                      <a href="https://esweb.irsc.edu/Mariner/security/repin1.jsp"
                         target="_new"
                         class="pull-right need-help">
                        Forgot your student password?
                      </a>
                      <span class="clearfix"></span>
                      <input type="hidden" name="new_loc" value="" />
                  </form>
              </div>
          </div>
      </header>

      <!-- Services Section -->
      <section id="support" class="bg-irsc-dark-grey">
          <div class="row">
              <div class="col-lg-4 col-sm-6 text-center">
                  <%-- <a href="http://bbsupport.irsc.edu/ics/support/default.asp?deptID=8616" alt="Blackboard Student Support" target="_blank"> --%>
                  <a href="https://help.edusupportcenter.com/shplite/IRSC/home" alt="Blackboard Student Support" target="_blank">
                    <span class="glyphicon glyphicon-question-sign help"></span>
                    <h3 class="accent-silver">Student Support</h3>
                    <p>Please take a look at our Knowledge base with 24/7 support.</p>
                  </a>
              </div>
              <div class="col-lg-4 col-sm-6 text-center">
                <a href="http://virtualcampusfacultysupport.weebly.com/" target="_blank">
                  <span class="glyphicon glyphicon-info-sign help"></span>
                  <h3 class="accent-silver">Faculty Support</h3>
                  <p>Faculty, please review the following support information.</p>
                </a>
              </div>
              <div class="col-lg-4 col-sm-6 text-center">
                  <a href="http://virtualcampus.irsc.edu/" target="_blank">
                    <span class="glyphicon glyphicon-exclamation-sign help"></span>
                    <h3 class="accent-silver">Virtual Campus</h3>
                    <p>Please visit the Virtual Campus website to get more information.</p>
                  </a>
              </div>
          </div>
      </section>



      <div id="bb-messages" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">

            <div class="modal-header">
              <h4 class="js-title-step"></h4>
            </div>

            <div id="bb-messages-body" class="modal-body"></div>

            <div id="multiple-alerts" class="modal-footer hidden">
              <button type="button" class="btn btn-default js-btn-step pull-left" data-orientation="cancel" data-dismiss="modal"></button>
              <button type="button" class="btn btn-warning js-btn-step" data-orientation="previous"></button>
              <button type="button" class="btn btn-success js-btn-step" data-orientation="next"></button>
            </div>

            <div id="single-alert" class="modal-footer hidden">
              <button type="button" class="btn btn-default js-btn-step pull-right" data-orientation="cancel" data-dismiss="modal"></button>
            </div>

          </div>
        </div>
      </div>

      <section id="announcements" class="bg-light-gray">
          <div class="container">
              <loginUI:systemAnnouncements maxItems="5" />
          </div>
      </section>

      <footer>
          <div class="container">
              <div class="row" class="bg-irsc-dark-grey">
                  <div class="col-md-4">
                      <span class="copyright">&copy;<script>document.write(new Date().getFullYear())</script> Indian River State College</span>
                  </div>
                  <div class="col-md-4" style="text-align: center;">
                      <ul class="list-inline social-buttons">
                          <li>
                            <a href="https://twitter.com/VCIRSC" target="_blank"><i class="fa fa-twitter"></i>
                            </a>
                          </li>
                          <li><a href="https://www.facebook.com/VCIRSC/" target="_blank"><i class="fa fa-facebook"></i>
                          </a>
                          </li>
                      </ul>
                  </div>
                  <div class="col-md-4">
                      <ul class="list-inline quicklinks">
                          <li>
                            <a href="http://access.blackboard.com/" target="_blank">Accessibility Information</a>
                          </li>
                          <!--<li><a href="javascript:showCopyrightDetail()">Installation Details</a></li>-->
                      </ul>
                  </div>
              </div>
          </div>
      </footer>

      <script id="bb-messages-step" type="text/template">
        <div class="row hide" data-step="::step::" data-title="::title::">
            <div class="jumbotron">::message::</div>
        </div>
      </script>

      <!-- Plugin JavaScript -->
      <script src="https://blackboard.irsc.edu/bbcswebdav/xid-1784710_1"></script>
      <script src="https://blackboard.irsc.edu/bbcswebdav/xid-1783387_1"></script>
      <script src="https://blackboard.irsc.edu/bbcswebdav/xid-1783382_1"></script>
      <script src="https://irsc.blackboard.com/bbcswebdav/xid-6232604_1"></script>

      <!-- Custom Theme JavaScript -->
      <script src="https://blackboard.irsc.edu/bbcswebdav/xid-1783379_1"></script>
      <script>
        $(document).ready(function(){
          var $alerts = $("#loginAnnouncements > ul > li strong:contains('::alert::')");
          if($alerts.length != 0){
            if ($alerts.length >= 1) {
              $('#alerts').removeClass('hidden');
              $('#alerts-count').text($alerts.length);
            }
            if ($alerts.length == 1) {
              $('#single-alert').removeClass('hidden');
            } else if ($alerts.length > 1) {
              $('#multiple-alerts').removeClass('hidden');
            }

            var $bbMessagesBody = $('#bb-messages-body');
            var $bbMessagesStep = $('#bb-messages-step');
            // console.log($alerts);
            $alerts.each(function(index){
              var title = $(this).text().replace('::alert::', '');
              var message = $(this).siblings('.vtbegenerated').html();
              $(this).text(title);

              // grab a copy of the template
              var $tmp = $.parseHTML(
                $bbMessagesStep.html()
                  .replace('::step::', (index + 1).toString())
                  .replace('::title::', title)
                  .replace('::message::', message)
                );

              $bbMessagesBody.append($tmp);
            });

            $('#bb-messages').modalSteps({
              btnCancelHtml: 'Cancel',
              btnPreviousHtml: 'Back',
              btnNextHtml: 'Next',
              btnLastStepHtml: 'Close'
            });

            $('#show-bb-maintenace').trigger('click');

          }

          if (window.navigator.userAgent.indexOf("Trident")) {
            $("header").addClass("header_ie");
            console.log(window.navigator.userAgent);
          }
        });
      </script>

  </body>
</html>
