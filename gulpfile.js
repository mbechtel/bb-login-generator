const gulp = require('gulp')
const sass = require('gulp-sass')
// const gls = require('gulp-live-server')
const server = require('gulp-server-livereload')
const autoPrefixer = require('gulp-autoprefixer')
const sourcemaps = require('gulp-sourcemaps')
const nunjucksRender = require('gulp-nunjucks-render')
const ts = require('gulp-typescript')
const data = require('gulp-data')
const path = require('path')
const sassInput = 'src/sass/**/*.scss'
const cssOutput = 'dist/css'
const htmlInput = 'src/templates/pages/**/*.+(html|nunjucks)'
const htmlOutput = 'dist'
const tsInput = 'src/typescript/**/*.ts'
const tsOutput = 'dist/js/app'
const sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compressed',
  includePaths: [
  ]
}
const jsVendors = [
  path.join(__dirname, 'node_modules', 'jquery', 'dist', 'jquery.min.js'),
  path.join(__dirname, 'node_modules', 'jquery.easing', 'jquery.easing.min.js'),
  path.join(__dirname, 'node_modules', 'magnific-popup', 'dist', 'jquery.magnific-popup.min.js'),
  path.join(__dirname, 'node_modules', 'scrollreveal', 'dist', 'scrollreveal.min.js'),
  path.join(__dirname, 'vendors', '**', '*.js'),
]
const cssVendors = [
  path.join(__dirname, 'node_modules', 'font-awesome', 'css', 'font-awesome.min.css'),
  path.join(__dirname, 'node_modules', 'animate.css', 'animate.min.css'),
  path.join(__dirname, 'node_modules', 'magnific-popup', 'dist', 'magnific-popup.css'),
]
const fontsVendors = [
  path.join(__dirname, 'node_modules', 'font-awesome', 'fonts', '**/*'),
]
const autoPrefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
}
const tasks = ['sass', 'html', 'copyJs', 'copyCss', 'copyFonts', 'typescript']
const tsProject = ts.createProject('tsconfig.json')

function getDataForFile(file){
  return require(file.path.slice(0,-8) + 'json')
}

// const server = gls.static('dist', 8000)

gulp.task('sass', function () {
  gulp.src(sassInput)
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(autoPrefixer(autoPrefixerOptions))
    .pipe(gulp.dest(cssOutput))
})

gulp.task('html', () =>
  gulp.src(htmlInput)
    .pipe(data(getDataForFile))
    .pipe(nunjucksRender({
      path: ['src/templates/']
    }))
    .pipe(gulp.dest(htmlOutput))
)

gulp.task('copyJs', () =>
  gulp.src(jsVendors).pipe(gulp.dest('dist/js'))
)

gulp.task('copyCss', () =>
  gulp.src(cssVendors).pipe(gulp.dest('dist/css'))
)

gulp.task('copyFonts', () =>
  gulp.src(fontsVendors).pipe(gulp.dest('dist/fonts'))
)

gulp.task('typescript', () =>
  tsProject.src()
          .pipe(tsProject())
          .js.pipe(gulp.dest(tsOutput))
)

gulp.task('watch', () => {
  gulp.watch(sassInput, ['sass'])
    .on('change', event => {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...')
    })

  gulp.watch(['src/templates/**/*', htmlInput], ['html'])
    .on('change', event => {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...')
    })
  gulp.watch(jsVendors, ['copyJs'])
    .on('change', event => {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...')
    })
  gulp.watch(tsInput, ['typescript'])
    .on('change', event => {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...')
    })
  // gulp.watch('dist/**/*.+(html|css|js|json)', file =>
  //   server.notify.apply(server, [file])
  // )
})

gulp.task('webserver', () =>
  // server.start()
  gulp.src('dist')
  .pipe(server({
    livereload: true,
    defaultFile: 'index.html',
    open: false
  }))
)

gulp.task('prod', tasks)

gulp.task('default', [].concat(tasks, ['webserver', 'watch']))
