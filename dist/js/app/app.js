(($) => {
    "use strict";
    $(document).ready(function () {
        const $topBar = $('.top-bar');
        const $controls = $topBar.find('[data-control]');
        $controls.each(function () {
            $(this).bind('click', function (e) {
                var $e = $(e.currentTarget);
                $controls.not($e)
                    .each(function () {
                    const $el = $(this);
                    console.log($el);
                    if ($el.attr('aria-expanded') == 'true') {
                        $el.attr('aria-pressed', 'false');
                        $el.attr('aria-selected', 'false');
                        $el.attr('aria-expanded', 'false');
                        $($el.data('control')).removeClass('active');
                        $($el.data('control')).attr('aria-expanded', 'false');
                        $el.removeClass('active');
                        if ($el.find('input')) {
                            $el.find('input').focus();
                        }
                    }
                });
                $e.toggleClass('active');
                $e.attr('aria-pressed', function (i, attr) {
                    return attr == 'true' ? 'false' : 'true';
                });
                $e.attr('aria-selected', function (i, attr) {
                    return attr == 'true' ? 'false' : 'true';
                });
                $e.attr('aria-expanded', function (i, attr) {
                    return attr == 'true' ? 'false' : 'true';
                });
                $($e.data('control')).attr('aria-expanded', function (i, attr) {
                    return attr == 'true' ? 'false' : 'true';
                });
                $($e.data('control')).toggleClass('active');
                $e.off('mouseenter mouseleave');
            });
        });
    });
    var randomImages = (function () {
        'use strict';
        return {
            init: function () {
                this.events();
            },
            events: function () {
                var images = [
                    'https://irsc.blackboard.com/bbcswebdav/xid-15273956_1',
                    'https://irsc.blackboard.com/bbcswebdav/xid-14463256_1',
                    'https://irsc.blackboard.com/bbcswebdav/xid-15273960_1',
                    'https://irsc.blackboard.com/bbcswebdav/xid-15273961_1',
                    'https://irsc.blackboard.com/bbcswebdav/xid-15273958_1',
                    'https://irsc.blackboard.com/bbcswebdav/xid-15686662_1',
                    'https://irsc.blackboard.com/bbcswebdav/xid-15686661_1',
                    'https://irsc.blackboard.com/bbcswebdav/xid-15273964_1'
                ];
                var s = document.createElement('style');
                s.id = 'random-Img';
                s.type = 'text/css';
                document.head.appendChild(s);
                var r = images[Math.floor(Math.random() * images.length)];
                s.textContent = '.bgimage{background:url(' + r + ') no-repeat center center fixed #eee;background-size:cover;-moz-background-size:cover;}';
            }
        };
    })();
    randomImages.init();
    $('a.page-scroll').bind('click', (event) => {
        let $anchor;
        let _tagName = event.target.tagName;
        if (['path', 'svg'].includes(_tagName)) {
            $anchor = $('#logo');
        }
        else {
            $anchor = $(event.target);
        }
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });
    let sr = ScrollReveal();
    sr.reveal('.sr-icons', {
        duration: 600,
        scale: 0.3,
        distance: '0px'
    }, 200);
    sr.reveal('.sr-button', {
        duration: 1000,
        delay: 200
    });
    sr.reveal('.sr-contact', {
        duration: 600,
        scale: 0.3,
        distance: '0px'
    }, 300);
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1]
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        }
    });
    $('img.svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        $.get(imgURL, function (data) {
            var $svg = jQuery(data).find('svg');
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
            }
            $img.replaceWith($svg);
        }, 'xml');
    });
})(jQuery);
